﻿//
//        XXXXXXXXXXXX            AllAboutHer                   BBBBBB
//       XX          XX                                         BB   BB
//      XX  XX    XX  XX          Raleigh Mobile .NET           BBBBBB
//     XX    XX  XX    XX         (Xamarin) Developers Group    BB   BB SSSSSS
//    XX      XXXX      XX                                      BBBBBB SS
//   XX        XX        XX       © 2018 Bobby Skinner                  SSSSS
//    XX      XXXX      XX        All rights reserved.                      SS
//     XX    XX  XX    XX                                              SSSSSS
//      XX  XX    XX  XX          BSD License
//       XX          XX
//        XXXXXXXXXXXX
//
//  Redistribution and use in source and binary forms are permitted
//  provided that the above copyright notice and this paragraph are
//  duplicated in all such forms and that any documentation,
//  advertising materials, and other materials related to such
//  distribution and use acknowledge that the software was developed
//  by Bobby Skinner. The name of Bobby Skinner may not be used to endorse 
//  or promote products derived from this software without specific prior 
//  written permission.
//
//  THIS SOFTWARE IS PROVIDED "AS IS" AND WITHOUT ANY EXPRESS OR
//  IMPLIED WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
//
//  Note: This license was specifically chosen to be incompatible with
//        the GPL license.
//
using System;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using AllAboutHer.Models;

namespace AllAboutHer.ViewModels
{
    public class EditPersonViewModel : ViewModelBase<Person>
    {
        /// <summary>
        /// The name of the person.
        /// </summary>
        /// <value>The name.</value>
        public string Name 
        { 
            get
            {
                return ModelObject?.Name;
            }

            set
            {
                if ( ModelObject == null )
                    ModelObject = new Person();
                
                if ( value != ModelObject.Name )
                {
                    ModelObject.Name = value;
                    NotifyPropertyChanged();
                }
            }
        }

        /// <summary>
        /// The birthday of the person.
        /// </summary>
        /// <value>The birthday.</value>
        public DateTime Birthday 
        { 
            get
            {
                return ModelObject.Birthday;
            }

            set
            {
                if ( ModelObject == null )
                    ModelObject = new Person();
                
                if ( value != ModelObject.Birthday )
                {
                    ModelObject.Birthday = value;
                    NotifyPropertyChanged();
                }
            } 
        }

        public EditPersonViewModel()
        {
            
        }

        public EditPersonViewModel( Person person )
        {
            ModelObject = person;
        }

        public async Task<(bool, string)> Save()
        {
            (bool success, string error) result = (false, null );

            if ( !String.IsNullOrEmpty( Name ) )
            {
                ResultSet<Person> saveResult = await Database.Current.SaveAsync<Person>( ModelObject );

                if ( saveResult.successful )
                    result.success = true;
                else
                {
                    result.success = false;
                    result.error = saveResult.Error;
                }
            }

            return result;
        }
    }
}
