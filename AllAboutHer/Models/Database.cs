﻿//
//        XXXXXXXXXXXX            AllAboutHer                   BBBBBB
//       XX          XX                                         BB   BB
//      XX  XX    XX  XX          Raleigh Mobile .NET           BBBBBB
//     XX    XX  XX    XX         (Xamarin) Developers Group    BB   BB SSSSSS
//    XX      XXXX      XX                                      BBBBBB SS
//   XX        XX        XX       © 2018 Bobby Skinner                  SSSSS
//    XX      XXXX      XX        All rights reserved.                      SS
//     XX    XX  XX    XX                                              SSSSSS
//      XX  XX    XX  XX          BSD License
//       XX          XX
//        XXXXXXXXXXXX
//
//  Redistribution and use in source and binary forms are permitted
//  provided that the above copyright notice and this paragraph are
//  duplicated in all such forms and that any documentation,
//  advertising materials, and other materials related to such
//  distribution and use acknowledge that the software was developed
//  by Bobby Skinner. The name of Bobby Skinner may not be used to endorse 
//  or promote products derived from this software without specific prior 
//  written permission.
//
//  THIS SOFTWARE IS PROVIDED "AS IS" AND WITHOUT ANY EXPRESS OR
//  IMPLIED WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
//
//  Note: This license was specifically chosen to be incompatible with
//        the GPL license.
using System;
using System.IO;
using System.Threading.Tasks;
using SQLite;

namespace AllAboutHer.Models
{
    public class Database
    {
        public static Database Current = null;

        protected SQLiteAsyncConnection connection = null;

        public Database()
        {
        }

        public async Task<Boolean> Open(String name)
        {
            Boolean result = false;

            Close();

            try
            {
                var databasePath = Path.Combine( 
                            Environment.GetFolderPath( 
                              Environment.SpecialFolder.MyDocuments), name);
                Console.WriteLine("Updating schema updated at " + databasePath);
                connection = new SQLiteAsyncConnection(databasePath);

                await CreateTables();
                Current = this;

                result = true;
                Console.WriteLine("Schema updated at " + databasePath);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception: " + ex);
            }

            return result;
        }

        public void Close()
        {
            connection = null;
        }

        public async Task CreateTables()
        {
            // create all the tables if needed
            await connection.CreateTableAsync<Person>();
            await connection.CreateTableAsync<Category>();
            await connection.CreateTableAsync<Item>();
        }

        //public ResultSet<T> Save<T>( T databaseObject ) where T : IDatabaseObject
        //{
        //    ResultSet<T> result = null;

        //    Task<ResultSet<T>> task = SaveAsync<T>( databaseObject );

        //    result = task.GetAwaiter().GetResult();

        //    return result;
        //}

        async public Task<ResultSet<T>> SaveAsync<T>( T databaseObject ) where T : IDatabaseObject
        {
            ResultSet<T> result = new ResultSet<T>();
            try
            {
                if (String.IsNullOrEmpty(databaseObject.PrimaryKey()))
                {
                    Guid guid = Guid.NewGuid();
                    databaseObject.SetPrimaryKey(guid.ToString());
                }

                if (await connection.InsertOrReplaceAsync(databaseObject) > 0 )
                {
                    result.Data.Add( databaseObject );

                    Console.WriteLine("Saved:  {0} ({1})",
                                       databaseObject.PrimaryKey(), databaseObject.GetType());
                }
                else
                {
                    result.Error = String.Format( "Could not save {0}", databaseObject.GetType() );
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine( "Exception: {0}", ex );
                result.Error = ex.ToString();
            }

            return result;
        }

        public async Task<ResultSet<Category>> GetCatagories()
        {
            AsyncTableQuery<Category> query = connection.Table<Category>().OrderBy(y => y.Name);
            ResultSet<Category> result = new ResultSet<Category>(await query.ToListAsync());

            return result;
        }

        public async Task<ResultSet<Person>> GetPeople()
        {
            AsyncTableQuery<Person> query = connection.Table<Person>().OrderBy(y => y.Name);
            ResultSet<Person> result = new ResultSet<Person>(await query.ToListAsync());

            return result;
        }


        public async Task<ResultSet<Item>> GetItems( Person person )
        {
            AsyncTableQuery<Item> query = null;

            if ( person == null )
                query = connection.Table<Item>().OrderBy(y => y.Name);
            else
                query = connection.Table<Item>().Where( x => x.PersonKey == person.PersonKey ).OrderBy(y => y.Name);

            ResultSet<Item> result = new ResultSet<Item>(await query.ToListAsync());

            return result;
        }
    }
}
