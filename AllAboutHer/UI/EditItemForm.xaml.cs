﻿//
//        XXXXXXXXXXXX            AllAboutHer                   BBBBBB
//       XX          XX                                         BB   BB
//      XX  XX    XX  XX          Raleigh Mobile .NET           BBBBBB
//     XX    XX  XX    XX         (Xamarin) Developers Group    BB   BB SSSSSS
//    XX      XXXX      XX                                      BBBBBB SS
//   XX        XX        XX       © 2018 Bobby Skinner                  SSSSS
//    XX      XXXX      XX        All rights reserved.                      SS
//     XX    XX  XX    XX                                              SSSSSS
//      XX  XX    XX  XX          BSD License
//       XX          XX
//        XXXXXXXXXXXX
//
//  Redistribution and use in source and binary forms are permitted
//  provided that the above copyright notice and this paragraph are
//  duplicated in all such forms and that any documentation,
//  advertising materials, and other materials related to such
//  distribution and use acknowledge that the software was developed
//  by Bobby Skinner. The name of Bobby Skinner may not be used to endorse 
//  or promote products derived from this software without specific prior 
//  written permission.
//
//  THIS SOFTWARE IS PROVIDED "AS IS" AND WITHOUT ANY EXPRESS OR
//  IMPLIED WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
//
//  Note: This license was specifically chosen to be incompatible with
//        the GPL license.
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AllAboutHer.Models;
using Xamarin.Forms;

namespace AllAboutHer.UI
{
    public partial class EditItemForm : ContentPage
    {
        public Person CurrentPerson = null;
        private Item _currentItem = null;

        public Item CurrentItem 
        {
            get 
            {
                if (_currentItem == null)
                    _currentItem = new Item();

                _currentItem.Name = Name.Text;
                _currentItem.Details = Details.Text;
                _currentItem.Color = Color.Text;
                _currentItem.Size = Size.Text;

                return _currentItem;
            }

            set 
            {
                _currentItem = value;

                if ( _currentItem != null )
                {
                    Name.Text = _currentItem.Name;
                    Details.Text = _currentItem.Details;
                    Color.Text = _currentItem.Color;
                    Size.Text = _currentItem.Size;
                }
            }
        }

        public EditItemForm()
        {
            InitializeComponent();

            // Create Save toolbar button
            ToolbarItem toolBarButton = new ToolbarItem("Save", null, () => { });
            ToolbarItems.Add(toolBarButton);
            toolBarButton.Clicked += SaveButtonPressed;

            CurrentItem = CurrentItem;
        }

        public async void SaveButtonPressed(object sender, EventArgs args)
        {
            if (await Save())
                await Navigation.PopAsync();
        }

        public async Task<bool> Save()
        {
            bool result = false;

            if (CurrentPerson != null)
            {
                if (!String.IsNullOrEmpty(Name.Text))
                {
                    ResultSet<Item> saveResult = await CurrentPerson.Add( CurrentItem );

                    if (saveResult.successful)
                        result = true;
                    else
                        await DisplayAlert("Error", "Could not add that item: " + saveResult.Error, "OK");
                }
            }

            return result;
        }
    }
}
