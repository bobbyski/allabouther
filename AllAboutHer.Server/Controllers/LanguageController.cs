﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace AllAboutHer.Server.Controllers
{
    [Route("api/[controller]")]
    public class LanguageController : Controller
    {
        private List<String> data = new List<string>( new string[] 
               { 
                    "Swift",
                    "C#",
                    "Objective-C",
                    "C++",
                    "Kotlin",
                    "Java",
                    "C",
                    "Pascal",
                    "BASIC",
                    "Assembly",
                    "Go",
                    "Dart",
                    "Lua",
                    "Objective-J",
                    "Smalltalk",
                    "JavaScript",
                    "F#",
                    "Ruby",
                    "Haskell",
                    "CoffeeScript",
                    "PHP",
                    "RPG",
                    "COBOL",
                    "Fortran",
                    "Pearl",
                    "Scala",
                    "Python" 
               } );

        // GET api/values
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return data.ToArray();
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            string result = "invalid request";

            if (id >= 0 && id < data.Count)
                result = data[id];
                
            return result;
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
